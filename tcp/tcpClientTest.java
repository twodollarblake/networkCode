import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class tcpClientTest {

    public static void main(String[] args) throws Exception
    {
        String ip = "localhost";
        int port = 9999;
        Socket s = new Socket(ip, port);

        OutputStreamWriter os = new OutputStreamWriter(s.getOutputStream());
        PrintWriter out = new PrintWriter(os);

        Scanner in = new Scanner(System.in);
        String str = "Blake Eastman\n";
        while (true) {
            out.print(in.nextLine()+"\n");
            os.flush();
            System.out.println("sent data to server");

            BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
            String nickname = br.readLine();

            System.out.println("C: Data from server: " + nickname);
        }
    }

}
