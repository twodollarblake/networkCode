import java.io.*;
import java.net.Socket;
import java.net.ServerSocket;

public class tcpServerTest {

    public static void main(String[] args) throws Exception
    {
        System.out.println("S: Server has started");
        ServerSocket ss = new ServerSocket(9999);///////

        System.out.println("S: Server is waiting for client request");
        Socket s = ss.accept();////////////

        System.out.println("S: client connected");

        BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));////////////
        OutputStreamWriter os = new OutputStreamWriter(s.getOutputStream());/////////////
        PrintWriter out = new PrintWriter(os);///////

        while (true) {
            String str = br.readLine();

            System.out.println("S: client data: " + str);

            String nickname = str.substring(0, 3);

            out.println(nickname);
            out.flush();
            System.out.println("S: data sent from server to client");
        }
    }

}
